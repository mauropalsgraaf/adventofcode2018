module Advent2 where

import qualified Data.Map.Strict as Map
import Data.List

main :: IO ()
main = do
    input <- getInput
    let computedValues = compute <$> input
    let nrOfTwo = length $ filter fst computedValues
    let nrOfThree = length $ filter snd computedValues
    let result = nrOfTwo * nrOfThree
    putStrLn $ show result

compute :: Box -> (Bool, Bool)
compute input = 
    compute' Map.empty input
    where compute' :: Map.Map Char Int -> Box -> (Bool, Bool)
          compute' map [] =
            let
                list = Map.assocs map
                anyMatch x = any (\y -> snd y == x) list
            in
                (anyMatch 2, anyMatch 3)
          compute' map (key:xs) = compute' (Map.insertWith (\_ new -> new + 1) key 1 map) xs

main2 :: IO ()
main2 = do
    input <- getInput
    let result = commonLettersOfClosestBoxes input
    putStrLn $ show result

type Box = String

commonLettersOfClosestBoxes :: [Box] -> String
commonLettersOfClosestBoxes boxes =
    let
        boxesProduct = uncurry findCommonLetters <$> (filter (\x -> fst x /= snd x) $ cartProd boxes boxes)
        commonLettersWithLength = ftupled length boxesProduct :: [(String, Int)]
        
        (res, _) = maximumBy findMax commonLettersWithLength
    in
        res
    
findCommonLetters :: Box -> Box -> String
findCommonLetters [] [] = []
findCommonLetters (x:xs) (x':xs')
    | x == x' = x : findCommonLetters xs xs'
    | otherwise = findCommonLetters xs xs'

getInput :: IO [Box]
getInput = lines <$> readFile "Advent2.txt"

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

ftupled :: Functor f => (a -> b) -> f a -> f (a, b)
ftupled f = fmap (\x -> (x, f x))

findMax :: (String, Int) -> (String, Int) -> Ordering
findMax (_, x) (_, x')
    | x > x' = GT
    | x < x' = LT
    | otherwise = EQ
          