{-# LANGUAGE NamedFieldPuns #-}

module Advent3 where

import Data.Foldable
import qualified Data.Map.Strict as M
import qualified Data.Set as S

main :: IO ()
main = do
    input <- getInput
    let parsedInput = parseInput <$> input
    let resultMap = foldl' addPositionToMap M.empty parsedInput
    let result = length $ M.toList $ M.filter (\x -> x >= 2) resultMap
    putStrLn $ show result

main2 :: IO ()
main2 = do
    input <- getInput
    let parsedInput = parseInput <$> input
    let (set, map) = foldl' addPositionToSetMap (S.empty, M.empty) parsedInput
    putStrLn $ show $ head $ toList set -- get the first element from the set (there is only one)

type Count = Int
type Id = Int
type X = Int
type Y = Int

data Position = Position { posId :: Int, x :: X, y :: Y, xn :: Int, yn :: Int } deriving (Show, Eq)

parseInput :: String -> Position
parseInput input =
    let
        (id, _) = getUntil (tail input) ' '

        inputWithoutId = tail $ tail $ dropWhile (\x -> x /= '@') input

        (x, leftOver) = getUntil inputWithoutId ','
        (y, leftOver1) = getUntil leftOver ':'
        (_, leftOver2) = getUntil leftOver1 ' '
        (xn, yn) = getUntil leftOver2 'x'
    in
        Position { posId = toInt id, x = toInt x, y = toInt y, xn = toInt xn, yn = toInt yn }

type SetMap = (S.Set Id, M.Map (X, Y) Id)

addPositionToSetMap :: SetMap -> Position -> SetMap
addPositionToSetMap (set, map) (Position { posId, x, y, xn, yn }) =
    let
        addId ::  Id -> (X, Y) -> ((X, Y), Id)
        addId id pair = (pair, id)

        -- generate all pairs and add the position id
        pairs = addId posId <$> [(x', y') | x' <- [x..x+xn-1], y' <- [y..y+yn-1]]
        
        -- insert the position id to the set
        newSet = S.insert posId set

        -- get all overwritten ids including itself and the updated map
        (overwrittenIds, map') = foldl' addPairToMap' ([], map) pairs
    in
        -- return the updated map and the set with all items that are overwritten removed
        (foldl' (\b a -> S.delete a b) newSet overwrittenIds, map')

addPairToMap' :: ([Id], M.Map (X, Y) Id) -> ((X, Y), Id) -> ([Id], M.Map (X, Y) Id)
addPairToMap' (overwrittenIds, map) (pair, posId) = 
    maybe 
        -- if the element is not foun, add it to the map
        (overwrittenIds, M.insert pair posId map)
        -- if it's found, add the new one to the map and add the overwritten ids
        (\foundId -> (foundId : posId : overwrittenIds, M.insert pair posId map))
        (M.lookup pair map)









addPositionToMap :: M.Map (X, Y) Count -> Position -> M.Map (X, Y) Count
addPositionToMap map (Position { x, y, xn, yn }) = 
    let
        pairs = [(x', y') | x' <- [x..x+xn-1], y' <- [y..y+yn-1]]
    in
        foldl' addPairToMap map pairs

addPairToMap :: M.Map (X, Y) Count -> (X, Y) -> M.Map (X, Y) Count
addPairToMap map pair = M.insertWith f pair 1 map
    where f :: Count -> Count -> Count
          f _ x = x + 1

toInt :: String -> Int
toInt s = read s

getUntil :: String -> Char -> (String, String)
getUntil input c = (takeWhile (\x -> x /= c) input, tail $ dropWhile (\x -> x /= c) input)

getInput :: IO [String]
getInput = lines <$> readFile "Advent3.txt"