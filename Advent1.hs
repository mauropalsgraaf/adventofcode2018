module Advent1 where

import Control.Monad.State
import Data.Foldable (foldl')
import qualified Data.Set as Set

main :: IO ()
main = do
    input <- getInput
    let result = foldl' calculate 0 input
    putStrLn $ show result
    return ()

main2 :: S -> IO ()
main2 initialState = do
    input <- getInput
    let s@(_, result, _) = snd $ runState (mapM_ calculate2 input) initialState
    case result of
        Just x -> putStrLn $ show x
        -- Recursively continue with the state
        Nothing -> main2 s

main3 :: IO ()
main3 = do
    input <- getInput
    let res = calculate3 (Set.singleton 0) 0 $ cycle input
    putStrLn $ show res
    

emptyState :: S
emptyState = (Set.singleton 0, Nothing, 0)

getInput :: IO [String]
getInput = lines <$> readFile "Advent1.txt"

type S = (Set.Set Int, Maybe Int, Int)
    
calculate2 :: String -> State S ()
calculate2 x = state f
    where f :: S -> ((), S)
          -- if the state contains the result, return the state with the element that was found
          f s@(_, Just _, _) = ((), s)
          f (set, _, res) =
            let
                nextNumber = calculate res x 
            in
                if nextNumber `Set.member` set
                then ((), (Set.empty, Just nextNumber, nextNumber))
                else ((), (Set.insert nextNumber set, Nothing, nextNumber))

calculate3 :: Set.Set Int -> Int -> [String] -> Int
calculate3 set value xs
    | calculate value (head xs) `Set.member` set = calculate value (head xs)
    | otherwise = calculate3 (Set.insert (calculate value (head xs)) set) (calculate value (head xs)) xs

calculate :: Int -> String -> Int
calculate s ('-':xs) = s - read xs
calculate s ('+':xs) = s + read xs


